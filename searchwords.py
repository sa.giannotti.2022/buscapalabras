'''
Busca una palabra en una lista de palabras
'''

import sys
import sortwords

def search_word(word, words_list):

    found = False
    for x in words_list:
        if sortwords.equal(word, x):
            found = True
            test_index = words_list.index(x)

    if not found:
        raise Exception

    return test_index

def main():
    word = sys.argv[1]
    words_list = sys.argv[2:]

    if len(sys.argv) < 3:
        sys.exit('Se necesitan al menos 2 argumentos')
    try:
        ordered_list = sortwords.sort(words_list)
        position = search_word(word, words_list)
        sortwords.show(ordered_list)
        print(position)
    except Exception:
        sys.exit('No se ha encontrado la palabra')


if __name__ == '__main__':
    main()
